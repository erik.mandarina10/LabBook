// DHT sensor
// Connect pin 1 (Vcc/IN/5V/+/...) (on the left) of the sensor to +5V
// NOTE: If using a board with 3.3V logic like an Arduino Due connect pin 1
// to 3.3V instead of 5V!
// Connect pin 2 (S) of the sensor to whatever your DHTPIN is
// Connect pin 4 (3/GND/0/-/... on the right) of the sensor to GROUND

// REQUIRES the following Arduino libraries:
// - DHT Sensor Library: https://github.com/adafruit/DHT-sensor-library
// - Adafruit Unified Sensor Lib: https://github.com/adafruit/Adafruit_Sensor
#include <DHT.h>

// Defines DHT sensor type: DHT11 (blue) or DHT22 (white)
#define DHTTYPE DHT22
// Defines the number of the pin to which the DHT is connected to
#define DHTPIN 2
// Creates a dht named DHT entity by pin and type
DHT dht(DHTPIN, DHTTYPE);

// Defines the pin numbers to which the RGB LEDs are to connected to
// Humidity LEDs - waterdrop shape
int redPinH = 9;
int greenPinH = 10;
int bluePinH = 11;
// Temperature LED - sun shape
int redPinT = 3;
int greenPinT = 5;
int bluePinT = 6;

void setup()
{
  // Set LED pins to output
  pinMode(redPinH, OUTPUT);
  pinMode(greenPinH, OUTPUT);
  pinMode(bluePinH, OUTPUT); 
  pinMode(redPinT, OUTPUT);
  pinMode(greenPinT, OUTPUT);
  pinMode(bluePinT, OUTPUT);
  // Start serial output for serial monitor
  Serial.begin(9600);
  // Start DHT sensor
  dht.begin();
}

void loop() {
  // Wait a few seconds between measurements.
  delay(2000);
  
  // Read relative humidity as procentage and save it as "h"
  float h = dht.readHumidity();
  // Read temperature as Celsius and save it as "t"
  float t = dht.readTemperature();

  // Print sensor values to serial monitor, for debugging
  Serial.print("Humidity: ");
  Serial.print(h);
  Serial.println(" %\t");
  Serial.print("Temperature: ");
  Serial.print(t);
  Serial.println(" °C ");

  // if Humidity is below 20% - turn on RED, if its above 60% - BLUE, if it's between - GREEN
  // Write in desired RED humidity LED threshold here
  if (h < 20){                    
    digitalWrite(redPinH, 255);
    digitalWrite(greenPinH, 0);
    digitalWrite(bluePinH, 0);
    }
  // Write in desired BLUE humidity LED threshold here
  else if (h > 60){
    digitalWrite(redPinH, 0);
    digitalWrite(greenPinH, 0);
    digitalWrite(bluePinH, 255);
    }
  else {
    digitalWrite(redPinH, 0);
    digitalWrite(greenPinH, 255);
    digitalWrite(bluePinH, 0);
    }

 // if Temperature is below 15°C - turn on BLUE, if its above 25°C - RED, if it's between - GREEN
 // Write in desired BLUE temperature LED threshold here
 if (t < 15){
    digitalWrite(redPinT, 0);
    digitalWrite(greenPinT, 0);
    digitalWrite(bluePinT, 255);
    }
  // Write in desired RED temperature LED threshold here
  else if (t > 25){
    digitalWrite(redPinT, 255);
    digitalWrite(greenPinT, 0);
    digitalWrite(bluePinT, 0);
    }
  else {
    digitalWrite(redPinT, 0);
    digitalWrite(greenPinT, 255);
    digitalWrite(bluePinT, 0);
    }
}
