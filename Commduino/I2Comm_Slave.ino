#include <Wire.h> 

/*
Connect 2 Arduino-type devices together with 3 pins: GND, SDA and SCL:
Uno, Pro Mini, Nano, Lilypad: A4 (SDA), A5 (SCL)
Mega, Due: 20 (SDA), 21 (SCL)
Leonardo, Yun: 2 (SDA), 3 (SCL)
ESP8266: D2 (SDA), D1 (SCL)
 */

int s = 0;

void setup() {
  // Define the LED pin as Output
  pinMode (LED_BUILTIN, OUTPUT);
  // Start the I2C Bus as Slave on address 9
  Wire.begin(9); 
  // Attach a function to trigger when something is received.
  Wire.onReceive(receiveEvent);
  Serial.begin(9600);
}
void receiveEvent(int bytes) {
  s = Wire.read();    // read one character from the I2C
}
void loop() {
    Serial.print(F("Received State: "));
    Serial.println(s);
    digitalWrite(LED_BUILTIN, !s);
}
