#include <Wire.h>
#include <Servo.h>

/*
Connect 2 Arduino-type devices together with 3 pins: GND, SDA and SCL:
Uno, Pro Mini, Nano, Lilypad: A4 (SDA), A5 (SCL)
Mega, Due: 20 (SDA), 21 (SCL)
Leonardo, Yun: 2 (SDA), 3 (SCL)
ESP8266: D2 (SDA), D1 (SCL)
 */
 
Servo myservo;  // create servo object to control a servo

int h = 0;
int t = 0;
int ang = 0;

void setup() {
  // Start the I2C Bus as Slave on address 9
  Wire.begin(9); 
  // Attach a function to trigger when something is received.
  Wire.onReceive(receiveEvent);
  // attaches the servo on pin 9 to the servo object
  myservo.attach(9);
  Serial.begin(9600);
}
void receiveEvent(int bytes) {
  t = Wire.read();    // read one character from the I2C
  h = Wire.read();    // read one character from the I2C
  }
void loop() {
  Serial.print(F("Received Temperature: "));
  Serial.println(t);
  Serial.print(F("Received Humidity: "));
  Serial.println(h);
  ang = map(h, 0, 100, 0, 180);     // scale it to use it with the servo (value between 0 and 180)
  myservo.write(ang);                  // sets the servo position according to the scaled value
  Serial.print(F("Servo Angle: "));
  Serial.println(ang);
  Serial.println(F("************"));
  delay(1000);                           // waits for the servo to get there
}
