/*
Plant soil moisture sensor - Vlagomir

Created by Zavod Kersnikova, Lovrenc Košenina
https://kersnikova.org/

* moisture sensor
Connect pin 1 (Vcc/IN/5V/+/...) (on the left) of the sensor to +5V
NOTE: If using a board with 3.3V logic like an Arduino Due connect pin 1
to 3.3V instead of 5V!
Connect pin 2 (S) of the sensor to whatever your DHTPIN is
Connect pin 3 (3/GND/0/-/... on the right) of the sensor to GROUND

* RGB LED
connect LED module negative contact (-) t oGND
Connect the RGB of humidity LED to pins 9, 10, 11
*/

#define VLAG_PIN A0

// Defines the pin numbers to which the RGB LEDs are to connected to
int ledPinB = 9;
int ledPinG = 10;
int ledPinR = 11;

// Defines the moisture thresholds: 1 - 1024 ~= 0 - 100% soil moisture
// Below threshold1 RED, above GREEN, above threshold2 BLUE LED shines
const int threshold1 = 300;
const int threshold2 = 600;


void setup() {
  pinMode(VLAG_PIN, INPUT);
  pinMode(ledPinR, OUTPUT);
  pinMode(ledPinG, OUTPUT);
  pinMode(ledPinB, OUTPUT);
  Serial.begin(115200);
}

void loop() {
// Read sensor value and save it as "hum"
  int hum = analogRead(VLAG_PIN);

// Print sensor values to serial monitor, for debugging
  Serial.print("Humidity = "); Serial.println(hum);
  Serial.println("---------------------------");
  
// if Humidity is below 30% - turn on RED, if its above 50% - BLUE, if it's between - GREEN
// Write in desired RED LED threshold here
  if (hum > threshold2){
    digitalWrite(ledPinR, 255);
    digitalWrite(ledPinG, 0);
    digitalWrite(ledPinB, 0);
    }
// Write in desired BLUE LED threshold here
  else if (hum < threshold1){
    digitalWrite(ledPinR, 0);
    digitalWrite(ledPinG, 0);
    digitalWrite(ledPinB, 255);
    }
  else {
    digitalWrite(ledPinR, 0);
    digitalWrite(ledPinG, 255);
    digitalWrite(ledPinB, 0);
    }
  delay(1000);  
}